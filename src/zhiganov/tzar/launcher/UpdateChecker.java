package zhiganov.tzar.launcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

public class UpdateChecker {

	public static String os;
	public static String arch;
	public static ArrayList<Item> items = new ArrayList<Item>();

	public static void Run() {
		checkSystemInformation();
		readConfig();
		// Create directory if it doesnt exist yet
		new File("./lib").mkdir();
		updateFiles();
		getChangelog();
		MainFrame.log("Updating Finished");
		MainFrame.status("Updating Finished");
		MainFrame.enablePlay();
	}

	public static void updateFiles() {
		for (Item item : items) {
			if (item.isUpdate())
				item.updateFile();
		}
	}

	public static void readConfig() {
		try {
			String strLine;
			BufferedReader br =
					new BufferedReader(new InputStreamReader((UpdateChecker.class.getResourceAsStream("/config.xml"))));

			while ((strLine = br.readLine()) != null) {
				if (strLine.contains("type"))
					items.add(new Item(strLine));
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void getChangelog() {
		try {
			MainFrame.changelog.setText(IOUtils
					.toString(new URL("https://raw.githubusercontent.com/puruscor/Purus-Pasta/master/CHANGELOG.md")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void checkSystemInformation() {
		os = System.getProperty("os.name");
		arch = System.getProperty("os.arch");
		MainFrame.log("OS: " + os);
		MainFrame.log("arch: " + arch);
		os = os.toLowerCase();
		if (os.contains("win"))
			os = "Windows";
		else if (os.contains("lin"))
			os = "Linux";
		else if (os.contains("mac"))
			os = "Mac";
		else
			os = "OS not defined";
	}
}