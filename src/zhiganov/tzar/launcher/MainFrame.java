package zhiganov.tzar.launcher;

import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private static JPanel contentPane;
	private static JTextArea textAreaLog = new JTextArea();
	private static JLabel lblStatus = new JLabel();
	private static JProgressBar progressBar = new JProgressBar();
	private static JButton btnPlay = new JButton("Play");
	private static JCheckBox chkBoxArgs = new JCheckBox("Custom JVM args");
	private static JTextField textFieldArgs;
	public static JTextArea changelog = new JTextArea();
	public static JLabel lblServerUp = new JLabel("Initializing...");
	public static JLabel lblPlayerCount = new JLabel("Initializing...");

	public static void run() {
		try {
			MainFrame frame = new MainFrame();
			frame.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public MainFrame() {
		super("Purus Pasta Launcher");
		setIconImage(Toolkit.getDefaultToolkit().getImage(MainFrame.class.getResource("/icon.png")));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 434, 361);
		contentPane.add(tabbedPane);

		JPanel tabMain = new JPanel();
		tabbedPane.addTab("Main", null, tabMain, null);
		btnPlay.setBounds(0, 301, 429, 32);
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				LaunchClient.launchClient();
			}
		});
		tabMain.setLayout(null);

		textFieldArgs = new JTextField();
		textFieldArgs.setEnabled(Main.prefs.getBoolean("chkBoxArgs", false));
		textFieldArgs.setBounds(10, 216, 409, 19);
		tabMain.add(textFieldArgs);
		textFieldArgs.setColumns(10);
		textFieldArgs.setText(Main.prefs.get("launchArgs", ""));

		lblStatus.setText("Initializing...");
		lblStatus.setBounds(0, 237, 419, 32);
		tabMain.add(lblStatus);
		lblStatus.setBackground(Color.WHITE);
		lblStatus.setHorizontalAlignment(SwingConstants.CENTER);

		btnPlay.setEnabled(false);
		tabMain.add(btnPlay);
		progressBar.setBounds(0, 270, 429, 32);

		progressBar.setMaximum(100);
		tabMain.add(progressBar);
		chkBoxArgs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.prefs.putBoolean("chkBoxArgs", chkBoxArgs.isSelected());
				textFieldArgs.setEnabled(chkBoxArgs.isSelected());
			}
		});
		chkBoxArgs.setSelected(Main.prefs.getBoolean("chkBoxArgs", false));
		chkBoxArgs.setBounds(6, 190, 154, 19);
		tabMain.add(chkBoxArgs);

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Main.prefs.put("launchArgs", textFieldArgs.getText());
			}
		});
		btnSave.setBounds(330, 242, 89, 23);
		tabMain.add(btnSave);

		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (UpdateChecker.os.equals("Windows"))
					textFieldArgs.setText(
							"javaw -Xms512m -Xmx1024m -jar hafen.jar -U http://game.havenandhearth.com/hres/ game.havenandhearth.com");
				else
					textFieldArgs.setText(
							"java -Xms512m -Xmx1024m -jar hafen.jar -U http://game.havenandhearth.com/hres/ game.havenandhearth.com");
				Main.prefs.put("launchArgs", textFieldArgs.getText());
			}
		});
		btnReset.setBounds(10, 242, 89, 23);
		tabMain.add(btnReset);

		lblServerUp.setBounds(10, 11, 409, 19);
		tabMain.add(lblServerUp);

		lblPlayerCount.setBounds(10, 33, 409, 19);
		tabMain.add(lblPlayerCount);

		JScrollPane changelogPane = new JScrollPane(changelog, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		changelogPane.setBounds(0, 0, 430, 270);
		tabbedPane.addTab("Changelog", null, changelogPane, null);

		changelog.setBounds(0, 0, 407, 269);

		textAreaLog.setEditable(false);
		textAreaLog.setLineWrap(true);

		JScrollPane logPane = new JScrollPane(textAreaLog, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		tabbedPane.addTab("Logs", null, logPane, null);

	}

	public static void log(String text) {
		textAreaLog.append(text + "\n");
	}

	public static void status(String text) {
		lblStatus.setText(text);
	}

	public static void setProgress(long p, int s) {
		progressBar.setMaximum(s);
		progressBar.setValue((int) p);
	}

	public static void enablePlay() {
		btnPlay.setEnabled(true);
	}
}