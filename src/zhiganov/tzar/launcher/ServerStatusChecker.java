package zhiganov.tzar.launcher;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerStatusChecker {

	public static void StartUpdaterThread() {
		Thread statusupdaterthread = new Thread(new Runnable() {
			public void run() {

				URL url;
				try {
					url = new URL("http://www.havenandhearth.com/mt/srv-mon"); // Url
																				// to
																				// connect
					HttpURLConnection con = (HttpURLConnection) url.openConnection(); // Initialize
																						// connection
					InputStream is = con.getInputStream(); // Inputstream from
															// url
					String stringBuf = ""; // Initialize buffer
					while (true) {
						int i = is.read(); // Reads next char as int
						if (i != 10) { // If its not line break, add char to
										// current buffer
							stringBuf = stringBuf + (char) i;
						} else { // Here we have one full line of text
							if (stringBuf.contains("users ")) {
								MainFrame.lblPlayerCount.setText("Hearthlings Playing: " + stringBuf.substring(6));
							} else if (stringBuf.contains("state hafen ")) {
								MainFrame.lblServerUp.setText("Server is " + stringBuf.substring(12));
							}
							stringBuf = "";
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		statusupdaterthread.start();
	}
}