package zhiganov.tzar.launcher;

public class LaunchClient {

	public static void launchClient() {
		try {
			MainFrame.log("Launching client...");
			MainFrame.status("Launching client...");
			if (Main.prefs.getBoolean("chkBoxArgs", false)) {
				if (UpdateChecker.os.equals("Windows"))
					Runtime.getRuntime().exec(Main.prefs.get("launchArgs",
							"javaw -Xms512m -Xmx1024m -jar hafen.jar -U http://game.havenandhearth.com/hres/ game.havenandhearth.com"));
				else
					Runtime.getRuntime().exec(Main.prefs.get("launchArgs",
							"java -Xms512m -Xmx1024m -jar hafen.jar -U http://game.havenandhearth.com/hres/ game.havenandhearth.com"));
			} else {
				if (UpdateChecker.os.equals("Windows"))
					Runtime.getRuntime().exec(
							"javaw -Xms512m -Xmx1024m -jar hafen.jar -U http://game.havenandhearth.com/hres/ game.havenandhearth.com");
				else
					Runtime.getRuntime().exec(
							"java -Xms512m -Xmx1024m -jar hafen.jar -U http://game.havenandhearth.com/hres/ game.havenandhearth.com");
			}
		} catch (Exception e) {
			e.printStackTrace();
			MainFrame.log("Launching client failed");
		}
	}
}