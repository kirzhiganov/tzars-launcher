package zhiganov.tzar.launcher;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.prefs.Preferences;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

public class Main {

	public static Preferences prefs = Preferences.userRoot().node("purus/pastalauncher");

	public static void main(String[] args) {
		updateLauncher();
		MainFrame.run();
		ServerStatusChecker.StartUpdaterThread();
		UpdateChecker.Run();
	}

	public static void updateLauncher() {
		System.out.println("Checking for updates for launcher...");
		try {
			String api =
					IOUtils.toString(new URL("https://api.github.com/repos/puruscor/pasta-launcher/releases/latest"));
			String size = api.substring(api.indexOf("\"size\":") + 7, api.indexOf(",\"download"));
			File launcherJar = new File("./lib/LauncherUpdate.zip");
			if (Integer.parseInt(size) != launcherJar.length()) {
				System.out.println("Update found! Downloading...");
				URL downloadUrl = new URL(
						api.substring(api.lastIndexOf("download_url\":\"") + 15, api.indexOf("\"}],\"tarball")));
				FileUtils.copyURLToFile(downloadUrl, launcherJar);
				System.out.println("Unzipping...");
				ZipFile zipFile = new ZipFile(launcherJar);
				Enumeration<? extends ZipEntry> entries = zipFile.entries();
				while (entries.hasMoreElements()) {
					ZipEntry entry = entries.nextElement();
					File entryDestination = new File(".", entry.getName());
					if (entry.isDirectory()) {
						entryDestination.mkdirs();
					} else {
						entryDestination.getParentFile().mkdirs();
						InputStream in = zipFile.getInputStream(entry);
						OutputStream out = new FileOutputStream(entryDestination);
						IOUtils.copy(in, out);
						IOUtils.closeQuietly(in);
						out.close();
					}
				}
				zipFile.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}