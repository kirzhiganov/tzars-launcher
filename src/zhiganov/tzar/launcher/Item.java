package zhiganov.tzar.launcher;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

public class Item {

	private String type;
	private String os;
	private String arch;
	private String link;
	private String file;
	private String name;
	private String gituser;
	private String gitrepo;

	public Item(String line) {
		this.type = getBetweenTags(line, "type");
		this.os = getBetweenTags(line, "os");
		this.arch = getBetweenTags(line, "arch");
		this.link = getBetweenTags(line, "link");
		this.file = getBetweenTags(line, "file");
		this.gituser = getBetweenTags(line, "gituser");
		this.gitrepo = getBetweenTags(line, "gitrepo");
		if (type.equals("github"))
			this.link = githubDownloadURL();
		this.name = getName();
	}

	public String getBetweenTags(String line, String tag) {
		int tagIndex1 = line.lastIndexOf("<" + tag + ">") + tag.length() + 2;
		int tagIndex2 = line.indexOf("</" + tag + ">");
		if (tagIndex1 < 0 || tagIndex2 < 0)
			return null;
		String strBetween = line.substring(tagIndex1, tagIndex2);
		return strBetween;
	}

	public String githubDownloadURL() {
		try {
			URL api;
			api = new URL("https://api.github.com/repos/" + gituser + "/" + gitrepo + "/releases/latest");
			String content = IOUtils.toString(api);
			String version = content.substring(content.indexOf("\"tag_name\":\"") + 12, content.indexOf("\",\"target"));
			String link =
					"https://github.com/" + gituser + "/" + gitrepo + "/releases/download/" + version + "/" + file;
			return link;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isUpdate() {
		// For Github
		if (type.equals("github")) {
			try {
				MainFrame.log("Checking updates for: " + name);
				MainFrame.status("Checking updates for: " + name);
				URL api = new URL("https://api.github.com/repos/" + gituser + "/" + gitrepo + "/releases/latest");
				String content = IOUtils.toString(api);
				String assetApi = content.substring(content.indexOf("build.zip\"},{\"url\":\"") + 20,
						content.indexOf(",\"name\":\"update.zip\""));
				assetApi = (assetApi.substring(0, assetApi.indexOf("\",\"id")));
				content = IOUtils.toString(new URL(assetApi));
				String size = content.substring(content.indexOf("\"size\":") + 7, content.indexOf(",\"download"));
				File foo = new File("./lib/" + name);
				if (Integer.parseInt(size) != foo.length()) {
					MainFrame.log("Update found for: " + name);
					return true;
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
			MainFrame.log("No updates found for: " + name);
			return false;
		}
		// For other stuff
		if (os != null && arch != null)
			if (!os.equals(UpdateChecker.os) || !arch.equals(UpdateChecker.arch))
				return false;
		try {
			MainFrame.log("Checking updates for: " + name);
			MainFrame.status("Checking updates for: " + name);
			URL obj = new URL(link);
			URLConnection conn = obj.openConnection();

			File foo = new File("./lib/" + name);

			if (conn.getLastModified() > foo.lastModified() || conn.getContentLength() != foo.length()) {
				MainFrame.log("Update found for: " + name);
				return true;
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		MainFrame.log("No updates found for: " + name);
		return false;
	}

	public void updateFile() {
		MainFrame.log("Updating " + name);
		MainFrame.status("Downloading: " + name);
		try {
			URL obj = new URL(link);
			URLConnection conn = obj.openConnection();
			ReadableByteChannel rbc = Channels.newChannel(new URL(link).openStream());
			FileOutputStream fos = new FileOutputStream(new File("./lib/" + name));
			long position = 0;
			int step = 20480;
			int size = conn.getContentLength();
			MainFrame.setProgress(position, size);
			while (position < size) {
				position += fos.getChannel().transferFrom(rbc, position, step);
				MainFrame.setProgress(position, size);
			}
			MainFrame.setProgress(0, size);
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (name.contains(".zip"))
			unZip("./lib/" + name);
	}

	public void unZip(String file) {
		MainFrame.log("Unzipping: " + name);
		try {
			ZipFile zipFile = new ZipFile(file);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				File entryDestination = new File(".", entry.getName());
				if (entry.getName().equals("pasta-res.jar") || entry.getName().equals("amber-res.jar")
						|| entry.getName().equals("l10n.jar") || entry.getName().equals("grid_ids.txt"))
					entryDestination = new File("./lib/", entry.getName());
				if (entry.isDirectory()) {
					entryDestination.mkdirs();
				} else {
					entryDestination.getParentFile().mkdirs();
					InputStream in = zipFile.getInputStream(entry);
					OutputStream out = new FileOutputStream(entryDestination);
					IOUtils.copy(in, out);
					IOUtils.closeQuietly(in);
					out.close();
				}
			}
			zipFile.close();
		} catch (Exception e) {

		}
	}

	public String getType() {
		return this.type;
	}

	public String getName() {
		String name;
		if (file != null)
			name = file;
		else
			name = FilenameUtils.getName(link);
		return name;
	}

	public String toString() {
		return type + os + arch + link + file;
	}
}